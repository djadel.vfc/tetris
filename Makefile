CC = g++
CFLAGS = -c -DLINUX -I. -LSDL/lib
LDFLAGS = -lSDL -lSDL_gfx -lSDL_mixer

all: bin/tetris

bin/tetris: obj/main.o obj/Jeu.o obj/Interface.o obj/Piece.o obj/Grille.o
	$(CC) obj/main.o obj/Jeu.o obj/Interface.o obj/Piece.o obj/Grille.o -o bin/tetris $(LDFLAGS)

obj/Jeu.o: include/Jeu.h src/Jeu.cpp src/Interface.cpp src/Grille.cpp include/Grille.h include/Interface.h
	$(CC) $(CFLAGS) src/Jeu.cpp -o obj/Jeu.o

obj/Interface.o: src/Interface.cpp include/Interface.h
	$(CC) $(CFLAGS) src/Interface.cpp -o obj/Interface.o

obj/Grille.o: src/Grille.cpp include/Grille.h
	$(CC) $(CFLAGS) src/Grille.cpp -o obj/Grille.o

obj/Piece.o: src/Piece.cpp include/Piece.h
	$(CC) $(CFLAGS) src/Piece.cpp -o obj/Piece.o

obj/main.o: include/Jeu.h src/Jeu.cpp main.cpp
	$(CC) $(CFLAGS) main.cpp -o obj/main.o

clean:
	rm -rf obj/* bin/*
