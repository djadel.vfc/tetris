#ifndef _PIECE_
#define _PIECE_

// 7 Types de pièces(forme), chaque pièce à 4 rotation
// on défini les pièces avec leur rotation dans un tableau à 4 dimension 

class Piece
{
public:

	int GetTypeBlock(int pPiece, int pRotation, int pX, int pY);
	int GetXInitialPosition (int pPiece, int pRotation);
	int GetYInitialPosition (int pPiece, int pRotation);
};

#endif // _PIECE_
