#ifndef _GRILLE_
#define _GRILLE_

#include "../include/Piece.h"

#define Largeur_Ligne_Grille 6  // largeur des 2 lignes qui délimitent la grille
#define Taille_Block 16         // hauteur et largeur de chaque bloque de piece
#define Position_Grille 320	// centrer la position de la grille 640/2 (a partir de la gauche de l'ecran)
#define Largeur_Grille 10
#define Hauteur_Grille 20
#define MARGE_VERTICAL 20		
#define MARGE_HORIZONTAL 20
#define PIECE_BLOCK 5		// Nombre de block horizontaux et verticaux d'une pièce de matrice


class Grille
{
	public:

		Grille(Piece *pPiece, int Hauteur_Ecran);
		int GetXPosEnPixels(int pPos);
		int GetYPosEnPixels(int pPos);
		bool BlockVide(int pX, int pY);
		bool MouvementPossible(int pX, int pY, int pPiece, int pRotation);
		void StorePiece(int pX, int pY, int pPiece, int pRotation);
		void SupprimerPlusieurLigne();
		bool IsGameOver();
		bool SupprimerPlusieurTF();
		int CptScore();

	private:

		enum { POS_Libre, POS_Rempli };			
		int mGrille [Largeur_Grille][Hauteur_Grille];
		Piece *mPiece;
		int mHauteur_Ecran;
		void InitGrille();
		void SupprimerLigne (int pY);
};

#endif // _Grille_
