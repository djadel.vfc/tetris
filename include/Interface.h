#ifndef _Interface_
#define _Interface_

#ifndef LINUX							
#include "../SDL/SDL_GfxPrimitives/SDL_gfxPrimitives_font.h"
#else
#include <SDL/SDL.h>									
#include "../SDL/SDL_GfxPrimitives/sdl_gfxprimitives.h"	
#endif
#pragma comment (lib, "SDL/lib/SDL.lib")
#pragma comment (lib, "SDL/SDL_GfxPrimitives/SDL_GfxPrimitives_Static.lib")


enum color {BLACK, RED, GREEN, BLUE, CYAN, MAGENTA, YELLOW, WHITE, COLOR_MAX}; // Colors


class Interface
{
public:

	Interface();

	void DessinerRect(int pX1, int pY1, int pX2, int pY2, enum color pC);
	void ClearScreen();
	int GetScreenHeight();
	int InitGraph();
	int Pollkey();
	int Getkey();
	int IsKeyDown(int pKey);
	void UpdateScreen();

};

#endif // _Interface_
