#ifndef _JEU_
#define _JEU_


#include "../include/Grille.h"
#include "../include/Piece.h"
#include "../include/Interface.h"
#include <time.h>


#define WAIT_TIME 700			


class Jeu
{
public:
	int mPosX, mPosY;	
	int mPiece, mRotation;		
	Jeu(Grille *pGrille, Piece *pPiece, Interface *pInterface, int Hauteur_Ecran);
	void DessinerJeu ();
	void GenererPieceSuivante();
	int GetRand (int pA, int pB);
	void InitJeu();
	void DessinerPiece (int pX, int pY, int pPiece, int pRotation);
	void DessinerGrille ();
private:

	int mHauteur_Ecran;		
	int mPosXSuiv, mPosYSuiv;		
	int mPieceSuiv, mRotationSuiv;

	Grille *mGrille;
	Piece *mPieces;
	Interface *mInterface;

	
};

#endif // _GAME_
