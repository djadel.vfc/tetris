#ifndef LINUX
#include <windows.h>
#endif
#include "../include/Jeu.h"

Jeu::Jeu(Grille *pGrille, Piece *pPiece, Interface *pInterface, int Hauteur_Ecran) 
{
	mHauteur_Ecran = Hauteur_Ecran;
	mGrille = pGrille;
	mPieces = pPiece;
	mInterface = pInterface;
	InitJeu ();
}


int Jeu::GetRand (int pA, int pB)
{
	return rand () % (pB - pA + 1) + pA;
}


void Jeu::InitJeu()
{
	// premiere piece
	srand ((unsigned int) time(NULL));
	
	mPiece = GetRand (0, 6);//un chiffre aleatoire entre 0 et 6, une des 7 pièce
	mRotation = GetRand (0, 3);// 4 rotation possible
	mPosX = (Largeur_Grille / 2) + mPieces->GetXInitialPosition (mPiece, mRotation);
	mPosY = mPieces->GetYInitialPosition (mPiece, mRotation); // ligne le plus en haut

	//  Piece Suivante
	
	mPieceSuiv = GetRand (0, 6);
	mRotationSuiv = GetRand (0, 3);
	mPosXSuiv = Largeur_Grille + 5;
	mPosYSuiv = 5;	
}


void Jeu::GenererPieceSuivante()
{
	// piece_Suiv qui devient pieceCourante
	mPiece = mPieceSuiv;
	mRotation = mRotationSuiv;
	mPosX = (Largeur_Grille / 2) + mPieces->GetXInitialPosition(mPiece, mRotation);
	mPosY = mPieces->GetYInitialPosition (mPiece, mRotation);


	mPieceSuiv = GetRand (0, 6);
	mRotationSuiv = GetRand (0, 3);
}



void Jeu::DessinerPiece (int pX, int pY, int pPiece, int pRotation)
{
	color mColor;			
	int mPixelsX = mGrille->GetXPosEnPixels(pX);
	int mPixelsY = mGrille->GetYPosEnPixels(pY);

	for (int i = 0; i < PIECE_BLOCK; i++)
	{
		for (int j = 0; j < PIECE_BLOCK; j++)
		{
			switch (mPieces->GetTypeBlock (pPiece, pRotation, j, i))
			{
				case 1: mColor = GREEN; break;
				case 2: mColor = BLUE; break;
			}
			
			if (mPieces->GetTypeBlock (pPiece, pRotation, j, i) != 0)
				mInterface->DessinerRect(mPixelsX + i * Taille_Block, 
									mPixelsY + j * Taille_Block, 
									(mPixelsX + i * Taille_Block) + Taille_Block - 1, 
									(mPixelsY + j * Taille_Block) + Taille_Block - 1, 
									RED);
		}
	}
}


void Jeu::DessinerGrille()
{
	// calcul la limite de grille en pixel
	
	int mX1 = Position_Grille - (Taille_Block * (Largeur_Grille / 2)) - 1;
	int mX2 = Position_Grille + (Taille_Block  * (Largeur_Grille / 2));
	int mY = mHauteur_Ecran - (Taille_Block * Hauteur_Grille);
	
	

	mInterface->DessinerRect (mX1 - Largeur_Ligne_Grille, mY, mX1, mHauteur_Ecran - 1, BLUE);
	mInterface->DessinerRect (mX2, mY, mX2 + Largeur_Ligne_Grille, mHauteur_Ecran - 1, BLUE);
	
	
	mX1 += 1;
	for (int i = 0; i < Largeur_Grille; i++)
	{
		for (int j = 0; j < Hauteur_Grille; j++)
		{	
		
			if (!mGrille->BlockVide(i, j))	
				mInterface->DessinerRect(mX1 + i * Taille_Block, 
										mY + j * Taille_Block, 
										(mX1 + i * Taille_Block) + Taille_Block - 1, 
										(mY + j * Taille_Block) + Taille_Block - 1, 
										RED);
		}
	}	
}



void Jeu::DessinerJeu()
{
	DessinerGrille();// dessiner la grille
	DessinerPiece(mPosX, mPosY, mPiece, mRotation);// dessiner la piece courante			
	DessinerPiece(mPosXSuiv, mPosYSuiv, mPieceSuiv, mRotationSuiv);// dessiner la piece suivante
}
