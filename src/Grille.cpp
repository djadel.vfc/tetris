#include "../include/Grille.h"

Grille::Grille(Piece *pPiece, int Hauteur_Ecran)
{
	mHauteur_Ecran = Hauteur_Ecran;
	mPiece = pPiece;
	InitGrille();
}


void Grille::InitGrille()
{	
	//initialiser la grille à vide
	for (int i = 0; i < Largeur_Grille; i++)
		for (int j = 0; j < Hauteur_Grille; j++)
			mGrille[i][j] = POS_Libre;
}

//A revoir
//Stock la piece dans la grille
void Grille::StorePiece (int pX, int pY, int pPiece, int pRotation)
{                //abscisse             // matrice 4 ligne
	for (int i1 = pX, i2 = 0; i1 < pX + PIECE_BLOCK; i1++, i2++)
	{
		         //ordonné                       // 4 colonne
		for (int j1 = pY, j2 = 0; j1 < pY + PIECE_BLOCK; j1++, j2++)
		{	
			if (mPiece->GetTypeBlock (pPiece, pRotation, j2, i2) != 0) // on garde que les 1 et 2 pes les 0	
				mGrille[i1][j1] = POS_Rempli;	
		}
	}
}

//Si la premier ligne est rempli fin de la partie
bool Grille::IsGameOver()
{
	for (int i=0; i < Largeur_Grille; i++)
	{
		// si 1 ère ligne pas vide donc gameOver
		if (mGrille[i][0] == POS_Rempli) return true;
	}

	return false;
}

//supprimer un ligne
void Grille::SupprimerLigne(int pY)
{
	for (int j = pY; j > 0; j--)
	{
		for (int i = 0; i < Largeur_Grille; i++)
		{
			mGrille[i][j] = mGrille[i][j-1]; // une seule ligne à supprimer
		}
	}	
}

//Supprimer Plusieur ligne
void Grille::SupprimerPlusieurLigne()
{
	for (int j = 0; j < Hauteur_Grille; j++)
	{
		int i = 0;
		while (i < Largeur_Grille)
		{
			if (mGrille[i][j] != POS_Rempli) break;
			i++;
		}

		if (i == Largeur_Grille) SupprimerLigne(j);
	}
}

// pour les collisions 
// si vide renvoie vrai
//faux sinon
bool Grille::BlockVide (int pX, int pY)
{
	if (mGrille [pX][pY] == POS_Libre)
		return true; 
	else return false;
}


int Grille::GetXPosEnPixels (int pPos)
{
	return  ( ( Position_Grille - (Taille_Block * (Largeur_Grille / 2)) ) + (pPos * Taille_Block) );
}

int Grille::GetYPosEnPixels (int pPos)
{
	return ( (mHauteur_Ecran - (Taille_Block * Hauteur_Grille)) + (pPos * Taille_Block) );
}

//Verifie si en peut bouger les pieces (si en n'est pas a la limite de la gride )
//return vrai si oui fausse sinon
bool Grille::MouvementPossible(int pX, int pY, int pPiece, int pRotation)
{

	for (int i1 = pX, i2 = 0; i1 < pX + PIECE_BLOCK; i1++, i2++)
	{	
		// on vérifie si on est en dehors ou en collision avec la grille
		for (int j1 = pY, j2 = 0; j1 < pY + PIECE_BLOCK; j1++, j2++)
		{	
			if (	i1 < 0 			||  // en dehors de la grille à gauche (sur la largeur)
				i1 > Largeur_Grille  - 1	||  // il faut pas dépasser par la droite la valeur 9
				j1 > Hauteur_Grille - 1)
			{
				if (mPiece->GetTypeBlock (pPiece, pRotation, j2, i2) != 0)
					return false; // on peux pas bouger		
			}

			if (j1 >= 0)	
			{
				if ((mPiece->GetTypeBlock (pPiece, pRotation, j2, i2) != 0) &&
					(!BlockVide (i1, j1))	)
					return false;
			}
		}
	}
	 // aucune collision avec grille ou pièce donc on bouge où on veux
	return true;
}

//revoie vrai si des ligne en ete supprimer 
//servira a calclue le score
bool Grille::SupprimerPlusieurTF()
{
	for (int j = 0; j < Hauteur_Grille; j++)
	{
		int i = 0;
		while (i < Largeur_Grille)
		{
			if (mGrille[i][j] != POS_Rempli) break;
			i++;
		}

		if (i == Largeur_Grille) return true;
	}
}

//Calcule et return le score 
int Grille::CptScore(){

	int score=0;
	while(SupprimerPlusieurTF()){
		score=score+1;
		}
	return score;

}




