#include "include/Jeu.h"
#include <iostream>
#include <fstream>
#include <string>
#include <SDL/SDL_mixer.h>
using namespace std;
#ifndef LINUX
#include <windows.h>
#endif

#ifndef LINUX
#else
int main()
#endif
{	

	int sc=0;	
	Interface mInterface;
	int mHauteur_Ecran = mInterface.GetScreenHeight();
	Piece mPiece;
	Grille mGrille (&mPiece, mHauteur_Ecran);
	Jeu mJeu (&mGrille, &mPiece, &mInterface, mHauteur_Ecran);
	unsigned long mTime1 = SDL_GetTicks();
	
	SDL_Init(SDL_INIT_AUDIO);
	if(Mix_OpenAudio(44100,MIX_DEFAULT_FORMAT,2,2048)<0){
		cout<<"Erreur son !"<<Mix_GetError()<<endl;
	}
	Mix_Music *bgm=Mix_LoadMUS("Tetris.wav");
	Mix_PlayMusic(bgm,-1);	
	
	

	while (!mInterface.IsKeyDown (SDLK_ESCAPE))
	{

		mInterface.ClearScreen (); 	
		mJeu.DessinerJeu ();			
		mInterface.UpdateScreen ();	


		int mKey = mInterface.Pollkey();

		switch (mKey)
		{
			case (SDLK_RIGHT): 
			{
				if (mGrille.MouvementPossible(mJeu.mPosX + 1, mJeu.mPosY, mJeu.mPiece, mJeu.mRotation))
					mJeu.mPosX++;
					break;
			}

			case (SDLK_LEFT): 
			{
				if (mGrille.MouvementPossible(mJeu.mPosX - 1, mJeu.mPosY, mJeu.mPiece, mJeu.mRotation))
					mJeu.mPosX--;	
				break;
			}

			case (SDLK_DOWN):
			{
				if (mGrille.MouvementPossible (mJeu.mPosX, mJeu.mPosY + 1, mJeu.mPiece, mJeu.mRotation))
					mJeu.mPosY++;	
				break;
			}

			case (SDLK_x):
			{
		
				while (mGrille.MouvementPossible(mJeu.mPosX, mJeu.mPosY, mJeu.mPiece, mJeu.mRotation)) { mJeu.mPosY++; }
	
				mGrille.StorePiece (mJeu.mPosX, mJeu.mPosY - 1, mJeu.mPiece, mJeu.mRotation);

				mGrille.SupprimerPlusieurLigne ();
				
				sc+=1;

				if (mGrille.IsGameOver())
				{
					mInterface.Getkey();
					exit(0);
				}

				mJeu.GenererPieceSuivante();

				break;
			}

			case (SDLK_z):
			{
				if (mGrille.MouvementPossible (mJeu.mPosX, mJeu.mPosY, mJeu.mPiece, (mJeu.mRotation + 1) % 4))
					mJeu.mRotation = (mJeu.mRotation + 1) % 4;

				break;
			}
		}

		unsigned long mTime2 = SDL_GetTicks();
		if ((mTime2 - mTime1) > WAIT_TIME)
		{
			if (mGrille.MouvementPossible (mJeu.mPosX, mJeu.mPosY + 1, mJeu.mPiece, mJeu.mRotation))
			{
				mJeu.mPosY++;
			}
			else
			{
				mGrille.StorePiece (mJeu.mPosX, mJeu.mPosY, mJeu.mPiece, mJeu.mRotation);

				mGrille.SupprimerPlusieurLigne ();

				if (mGrille.IsGameOver())
				{	
					mInterface.Getkey();
					exit(0);
				}

				mJeu.GenererPieceSuivante();
			}

			mTime1 = SDL_GetTicks();
		}
	     
	}
	

	Mix_FreeMusic(bgm);
	bgm=nullptr;
		
	ofstream Fichier;
	Fichier.open("Score.txt");				
	Fichier<<"votre Score : "<<sc;
	Fichier.close();
	Mix_Quit();
	

	return 0;
}
